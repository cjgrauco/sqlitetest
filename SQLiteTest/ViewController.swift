//
//  ViewController.swift
//  SQLiteTest
//
//  Created by Johan Graucob on 2015-08-23.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//
//code from:
//http://www.techotopia.com/index.php/An_Example_SQLite_based_iOS_8_Application_using_Swift_and_FMDB


import UIKit

class ViewController: UIViewController {

    //outlets
    @IBOutlet weak var buttonInsert: UIButton!
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var buttonDisplayRecords: UIButton!
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfAdress: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var labStatus: UILabel!
    @IBOutlet weak var tfSearch: UITextField!
    
    
    
    //global variables
    var databasePath = NSString()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // constructs a path to contacts database, and cleasts a NSFileManager
        let fileMngr = NSFileManager.defaultManager()
        let dirPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let docsDir = dirPath[0] as String
        databasePath = docsDir.stringByAppendingPathComponent("contacts.db")
        
        //No database exists
        if !fileMngr.fileExistsAtPath(databasePath as String){
            
            // creates a FMDatabase named contactDB
            let contactDB = FMDatabase(path: databasePath as String)
            
            //failed to create db
            if contactDB == nil {
                println("Error: \(contactDB.lastErrorMessage())")
            }
            // tries to open the db
            if contactDB.open(){
                //creates tables if the dont exist
                let sql_stmt = "CREATE TABLE IF NOT EXISTS CONTACTS ( ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ADRESS TEXT, PHONE TEXT)"
                
                //failed
                if !contactDB.executeStatements(sql_stmt){
                    println("Error: \(contactDB.lastErrorMessage())")
                    
            }
                //closes connection to db
             contactDB.close()
                
            }else{
                println("Error: \(contactDB.lastErrorMessage())")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    func resetFields(){
        tfName.text = ""
        tfAdress.text = ""
        tfPhone.text = ""
    }
    
    
    @IBAction func pushedInsert(sender: UIButton) {
        let contactDB = FMDatabase(path: databasePath as String)
        
        if contactDB.open() {
            let insertSQL = "INSERT INTO CONTACTS (name, adress, phone) VALUES ('\(tfName.text)', '\(tfAdress.text)', '\(tfPhone.text)')"
            
            let result = contactDB.executeUpdate(insertSQL, withArgumentsInArray: nil)
            
            if !result {
                labStatus.text = "failed"
                println("Error: \(contactDB.lastErrorMessage())")
                
            }else{
                labStatus.text = "added"
                resetFields()
                contactDB.close()
            }
            
        }else{
             println("Error: \(contactDB.lastErrorMessage())")
        }
        
    }


    @IBAction func pushedDelete(sender: UIButton) {
    }
    

    @IBAction func pushedSearch(sender: AnyObject) {
        
        let contactDB = FMDatabase(path: databasePath as String)
        if contactDB.open(){
            let query = "SELECT adress, phone, name FROM CONTACTS WHERE name = '\(tfSearch.text)'"
            
            let results:FMResultSet? = contactDB.executeQuery(query, withArgumentsInArray: nil)
            
            if results?.next() == true{
                
                tfAdress.text = results?.stringForColumn("adress")
                tfName.text = results?.stringForColumn("name")
                tfPhone.text = results?.stringForColumn("phone")
                labStatus.text = "found"
            }else{
                labStatus.text = "not found"
                resetFields()
            }
            contactDB.close()
        }else{
             println("Error: \(contactDB.lastErrorMessage())")
        }
    }

    
    
    
}

